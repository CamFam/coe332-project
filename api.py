import json
import flask
from flask import jsonify
from flask import Flask
from flask import request
from requests import get,post
import uuid
import redis
import jobs
import matplotlib.pyplot as plt

app = Flask(__name__)

#Jobs data
rd = redis.StrictRedis(host='172.17.0.1',port=6379,db=0)
#Jobs results
res = redis.StrictRedis(host='172.17.0.1',port=6379,db=3)

plots = redis.StrictRedis(host='172.17.0.1',port=6379,db=2)

#ENDPOINTS

# Data from a json file
data = json.load(open('NBA_Combine_Data.json', 'r'))

#home route
@app.route('/')
def NBA_Combine_Data(): 
    return jsonify(data)
	
def years():
    gathered_data = []
    for dat in data:
        if dat['Year'] not in gathered_data:
            gathered_data.append(dat['Year'])
    return gathered_data

years = years()

@app.route('/players')
def get_players():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['id'] : dat['Player']})
    return jsonify(gathered_data)

@app.route('/players/<string:name>')
def get_player(name):
    for dat in data:
        if dat['Player'].replace(" ", "").lower() == name.lower():
            return jsonify(dat)
    return '',404
			
@app.route('/years/<int:year>/players')
def get_year_players(year):
    if year not in years:
        return '', 404
    else:
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['id'] : dat['Player']})
        return jsonify(gathered_data)
		
@app.route('/addplayer', methods=['POST'])
def post_player():
    try:
        player = request.get_json(force=True)
        data.append(player)
        return 'Player has been added\n', 200
    except Exception as e:
        return json.dumps({'status':'Error','message':'Invalid JSON: {}.'.format(e)}), 400
			
@app.route('/years')
def get_years():
    gathered_data = []
    for dat in data:
        if dat['Year'] not in gathered_data:
            gathered_data.append(dat['Year'])
    return jsonify(gathered_data)

@app.route('/years/<int:year>')
def get_year(year):
    if year in years:
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append(dat)
        return jsonify(gathered_data)
    else:
        return '', 404

@app.route('/picks')
def get_picks():
    gathered_data = []
    for dat in data:
        if dat['Draft pick'] not in gathered_data:
            gathered_data.append(dat['Draft pick'])
    return jsonify(gathered_data)

@app.route('/picks/<int:pick>')
def get_pick(pick):
    if pick > 60:
        return '', 404
    else:	
        gathered_data = []
        for dat in data:
            if dat['Draft pick'] == pick:
                gathered_data.append(dat)
        return jsonify(gathered_data)

@app.route('/height')
def get_height():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : [dat['Height (No Shoes)'], dat['Height (With Shoes)']]})
    return jsonify(gathered_data)

@app.route('/years/<int:year>/height')
def get_year_height(year):
    if year not in years:
        return '', 404
    else:
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : [dat['Height (No Shoes)'], dat['Height (With Shoes)']]})
        return jsonify(gathered_data)

@app.route('/wingspan')
def get_wingspan():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : dat['Wingspan']})
    
    return jsonify(gathered_data)

@app.route('/years/<int:year>/wingspan')
def get_year_wingspan(year):
    if year not in years:
        return '', 404
    else:
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : dat['Wingspan']})
        return jsonify(gathered_data)

@app.route('/vertical')
def get_vertical():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : [dat['Vertical (Max)'], dat['Vertical (Max Reach)'], dat['Vertical (No Step)'], dat['Vertical (No Step Reach)'] ]})
    
    return jsonify(gathered_data)

@app.route('/years/<int:year>/vertical')
def get_year_vertical(year):
    if year not in years:
        return '', 404
    else:
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : [dat['Vertical (Max)'], dat['Vertical (Max Reach)'], dat['Vertical (No Step)'], dat['Vertical (No Step Reach)'] ]})
        return jsonify(gathered_data)

@app.route('/weight')
def get_weight():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : dat['Weight']})    
    return jsonify(gathered_data)

@app.route('/years/<int:year>/weight')
def get_year_weight(year):
    if year not in years:
        return '', 404
    else:		
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : dat['Weight']})
        return jsonify(gathered_data)

@app.route('/fat')
def get_fat():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : dat['Body Fat']})    
    return jsonify(gathered_data)

@app.route('/years/<int:year>/fat')
def get_year_fat(year):
    if year not in years:
        return '', 404		
    else:
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : dat['Body Fat']})
        return jsonify(gathered_data)

@app.route('/hand')
def get_hand():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : [dat['Hand (Length)'], dat['Hand (Width)']]})    
    return jsonify(gathered_data)

@app.route('/years/<int:year>/hand')
def get_year_hand(year):
    if year not in years:
        return '', 404
    else:		
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : [dat['Hand (Length)'], dat['Hand (Width)']]})
        return jsonify(gathered_data)

@app.route('/bench')
def get_bench():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : dat['Bench']})    
    return jsonify(gathered_data)

@app.route('/years/<int:year>/bench')
def get_year_bench(year):
    if year not in years:
        return '', 404
    else:		
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : dat['Bench']})
        return jsonify(gathered_data)

@app.route('/agility')
def get_agility():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : dat['Agility']})    
    return jsonify(gathered_data)
	
@app.route('/years/<int:year>/agility')
def get_year_agility(year):
    if year not in years:
        return '', 404
    else:		
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : dat['Agility']})
        return jsonify(gathered_data)
	
@app.route('/sprint')
def get_sprint():
    gathered_data = []
    for dat in data:
        gathered_data.append({dat['Player'] : dat['Sprint']})
    return jsonify(gathered_data)	

@app.route('/years/<int:year>/sprint')
def get_year_sprint(year):
    if year not in years:
        return '', 404
    else:		
        gathered_data = []
        for dat in data:
            if dat['Year'] == year:
                gathered_data.append({dat['Player'] : dat['Sprint']})    
        return jsonify(gathered_data)

#-----------------------------------------------------------------------------------------------------------
#Routes for processed jobs
@app.route('/wingspan/mean')
def get_mean_wingspan():
    command = 'calc_mean_wingspan()'
    start = ''
    end = ''
    job_dict = jobs.add_job(command,start,end)
    job_dict = json.dumps(job_dict)
    return job_dict+'\n', 200

@app.route('/wingspan/mean/results')
def get_mean_ws_res():
    try:
        results = res.hgetall('mean_ws')
        results = {key.decode('utf-8'):value.decode('utf-8') for key, value in results.items()}
    except:
        return 'This result has not been calculated yet!\n', 404

    result = json.dumps(results)
    if results == {}:
        return 'This result has not been calculated yet!\n', 404
    else:
        return result+'\n', 200

@app.route('/wingspan/mean/results/plot')
def get_mean_ws_plot():
    try:
        results = res.hgetall('mean_ws')
        results = {key.decode('utf-8'):value.decode('utf-8') for key, value in results.items()}
        x = []
        y = []
        for entry in results:
            x.append(int(entry))
            y.append(float(results[entry]))
        plt.hist(x,y)
        plt.xlabel('Year')
        plt.ylabel('Wingspan (in.)')
        plt.title('Average Wingspan of Drafted Players')
        plt.savefig('./fig.png')
        f_bytes = open('./fig.png','rb').read()
        plot.set('avg_ws',f_bytes)
        return '', 200
    except:
        return 'This result has not been calculated yet!\n', 404

@app.route('/weight/mean')
def get_mean_weight():
    command = 'calc_mean_weight()'
    start = ''
    end = ''
    job_dict = jobs.add_job(command,start,end)
    job_dict = json.dumps(job_dict)
    return job_dict+'\n', 200

@app.route('/weight/mean/results')
def get_mean_w_res():
    try:
        results = res.hgetall('mean_w')
        results = {key.decode('utf-8'):value.decode('utf-8') for key, value in results.items()}
    except:
        return 'This result has not been calculated yet!\n', 404

    result = json.dumps(results)
    if results == {}:
        return 'This result has not been calculated yet!\n', 404
    else:
        return result+'\n', 200

@app.route('/weight/mean/results/plot')
def get_mean_w_plot():
    try:
        results = res.hgetall('mean_w')
        results = {key.decode('utf-8'):value.decode('utf-8') for key, value in results.items()}
        x = []
        y = []
        for entry in results:
            x.append(int(entry))
            y.append(float(results[entry]))
        plt.hist(x,y)
        plt.xlabel('Year')
        plt.ylabel('Weight (lbs)')
        plt.title('Average Weight of Drafted Players')
        plt.savefig('./fig.png')
        f_bytes = open('./fig.png','rb').read()
        plot.set('avg_w',f_bytes)
        return '', 200
    except:
        return 'This result has not been calculated yet!\n', 404
#-----------------------------------------------------------------------------------------------------------
#Get job data
@app.route('/jobdata/<string:jid>')
def get_job_data(jid):
    try:
        results = rd.getall(jid)
        results = {key.decode('utf-8'):value.decode('utf-8') for key, value in results.items()}
        result = json.dumps(results)
        return result+'\n', 200
    except:
        'This job did not exist!\n', 404

###
@app.route('/jobs', methods=['POST'])
def jobs_api():
    try:
        job = request.get_json(force=True) #job will be a JSON object with start and end
    except Exception as e:
        return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
    
    return json.dumps(jobs.add_job(job['command'],job['start'], job['end']))

###
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
