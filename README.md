# COE332-Project

## Project Coordinators:
	Cameron Arellano 
	Troy Walton

This repository contains an api to provide data and analysis from the NBA Draft Combine

Deployment:
	After pulling the repo, 
		docker-compose up --build

USER:
	All Endpoints and curls are given in API_specs.txt

NOTES: 
	The worker was circumvented a bit by making endpoints that create and run jobs.  These endpoints are given in the spec sheet.  When a job is posted, the worker will read through it, but it serves no purpose currently.
	