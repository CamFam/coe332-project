import uuid
import json
import redis
import requests
import hotqueue
from hotqueue import HotQueue
### This contains all the functions associated with jobs

#Jobs data
rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
#Jobs results
res = redis.StrictRedis(host='172.17.0.1',port=6379,db=3)
q = HotQueue("queue", host='172.17.0.1', port=6379, db=1)
data = json.load(open('NBA_Combine_Data.json','r'))
plots = redis.StrictRedis(host='172.17.0.1',port=6379,db=2)

def _generate_jid():
    return str(uuid.uuid4())

def generate_job_key(jid):
    return 'job.{}'.format(jid)
	
def _instantiate_job(jid, status, command, start, end):
	#create a job object, will be used in function add_job
    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'command': command,
                'start': start,
                'end': end}
    else:
        return {'id': jid,
                'status': status.decode('utf-8'),
                'command': command.decode('utf-8'),
                'start': start.decode('utf-8'),
                'end': end.decode('utf-8')}

def _save_job(job_key, job_dict):
    #Save a job object in the Redis database.
    rd.hmset(job_key, job_dict)

def _queue_job(jid):
    #Add a job to the redis queue."""
    q.put(jid)

def add_job(command, start, end, status="submitted"):
    #Save/Queue a job.
    jid = _generate_jid()
    job_dict = _instantiate_job(jid, status, command, start, end)
    _save_job(jid, job_dict)
    _queue_job(jid)
    return job_dict

def update_job_status(jid, status):
    #Update the status of job with job id `jid` to status `status`.
    jid, status, command, start, end = rd.hmget(jid, 'id', 'status', 'command', 'start', 'end')
    job = _instantiate_job(jid, status, command, start, end)
    if job:
        job['status'] = status
        _save_job(jid, job)
    else:
        raise Exception()

def _save_results(label, results_dict):
    res.hmset(label, results_dict)

#Processing jobs

def yearss():
    gathered_data = []
    for dat in data:
        if dat['Year'] not in gathered_data:
            gathered_data.append(dat['Year'])
    return gathered_data

def calc_mean_wingspan():
    #years = requests.get('http://localhost:5000/years')
    years = yearss()
    ws_avg = {}
    for year in years:
        #url = 'http://localhost:5000/years/'+str(years.json()[y])+'/wingspan'
        #ws_data = requests.get(url)
        ws_data = []
        for dat in data:
            if dat['Year'] == year:
                ws_data.append(dat['Wingspan'])
        tot = 0
        temp = 0
        #for p in ws_data.json():
        for p in ws_data:
            #if str(ws_data.json()[p])!="" and float(ws_data.json()[p])!=0:
            if str(p) != '' and float(p) != 0:
                temp = temp + 1
                #tot = tot + float(ws_data.json()[p])
                tot = tot + float(p)
        avg = tot / temp
        #ws_avg.append({str(y):str(avg)})
        ws_avg[year] = str(avg)
    #put in database
    _save_results('mean_ws',ws_avg)
    return '', 200

def calc_mean_weight():
    years = yearss()
    w_avg = {}
    for year in years:
        w_data = []
        for dat in data:
            if dat['Year'] == year:
                w_data.append(dat['Weight'])
        tot = 0
        temp = 0
        for p in w_data:
            if str(p) != '' and float(p) != 0:
                temp = temp + 1
                tot = tot + float(p)
        avg = tot / temp
        w_avg[year] = str(avg)
    _save_results('mean_w',w_avg)
    return '', 200
